//
//  main.m
//  p04-ferrara
//
//  Created by Dylan Ferrara on 3/12/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

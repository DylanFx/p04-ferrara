//
//  GameScene.m
//  p04-ferrara
//
//  Created by Dylan Ferrara on 3/12/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene 

static const uint32_t flapperCategory = 1 << 0;
static const uint32_t worldCategory = 1 << 1;
static const uint32_t pipeCategory = 1 << 2;
static const uint32_t scoreCategory = 1 << 3;

static NSInteger const verticalPipeGap = 150;

- (id) initWithSize:(CGSize)size {
    // Setup your scene here
    if (self = [super initWithSize:size]) {
        
        _canRestart = NO;
        
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.0);
        self.physicsWorld.contactDelegate = self;
        
        _sky = [SKColor colorWithRed:113.0/255.0 green:197.0/255.0 blue:207.0/255.0 alpha:1.0];
        [self setBackgroundColor:_sky];
        
        _moving = [SKNode node];
        [self addChild:_moving];
        
        _pipes = [SKNode node];
        [_moving addChild:_pipes];
        
        _bars = [SKNode node];
        [_moving addChild:_bars];
        
        //Flapping Object
        SKTexture *flap1 = [SKTexture textureWithImageNamed:@"Face1"];
        flap1.filteringMode = SKTextureFilteringNearest;
        SKTexture *flap2 = [SKTexture textureWithImageNamed:@"Face2"];
        flap2.filteringMode = SKTextureFilteringNearest;
        
        SKAction *move = [SKAction repeatActionForever:[SKAction animateWithTextures:@[flap1, flap2] timePerFrame:0.2]];
        
        _flapper = [SKSpriteNode spriteNodeWithTexture:flap1];
        [_flapper setScale:2.0];
        _flapper.position = CGPointMake(self.frame.size.width / 4, CGRectGetMidY(self.frame));
        [_flapper runAction:move];
        
        _flapper.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:_flapper.size.height / 2];
        _flapper.physicsBody.dynamic = YES;
        _flapper.physicsBody.allowsRotation = NO;
        _flapper.physicsBody.categoryBitMask = flapperCategory;
        _flapper.physicsBody.collisionBitMask = worldCategory | pipeCategory;
        _flapper.physicsBody.contactTestBitMask = worldCategory | pipeCategory;
        
        [self addChild:_flapper];
        
        //Ground
        SKTexture *ground = [SKTexture textureWithImageNamed:@"Ground"];
        ground.filteringMode = SKTextureFilteringNearest;
        
        SKAction *moveGround = [SKAction moveByX:-ground.size.width*2 y:0 duration:0.02*ground.size.width*2];
        SKAction *resetGround = [SKAction moveByX:ground.size.width*2 y:0 duration:0];
        SKAction *moveGroundForver = [SKAction repeatActionForever:[SKAction sequence:@[moveGround, resetGround]]];
        
        for (int i = 0; i < 2 + self.frame.size.width / (ground.size.width * 2); ++i) {
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:ground];
            [sprite setScale:2.0];
            sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height / 2);
            [sprite runAction:moveGroundForver];
            [_moving addChild:sprite];
        }
        
        //Ground Physics Container
        SKNode *fakeGround = [SKNode node];
        fakeGround.position = CGPointMake(0, ground.size.height);
        fakeGround.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, ground.size.height*2)];
        fakeGround.physicsBody.dynamic = NO;
        fakeGround.physicsBody.categoryBitMask = worldCategory;
        [self addChild:fakeGround];
        
        //Skyline
        SKTexture *skyline = [SKTexture textureWithImageNamed:@"Skyline"];
        skyline.filteringMode = SKTextureFilteringNearest;
        
        SKAction *moveSky = [SKAction moveByX:-skyline.size.width*2 y:0 duration:0.1*skyline.size.width*2];
        SKAction *resetSky = [SKAction moveByX:skyline.size.width*2 y:0 duration:0];
        SKAction *moveSkyForver = [SKAction repeatActionForever:[SKAction sequence:@[moveSky, resetSky]]];
        
        for (int i = 0; i < 2 + self.frame.size.width / (skyline.size.width * 2); ++i) {
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:skyline];
            [sprite setScale:2.0];
            sprite.zPosition = -20;
            sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height / 2 + ground.size.height * 2);
            [sprite runAction:moveSkyForver];
            [_moving addChild:sprite];
        }
        
        //Pipes
        _pipeTexture1 = [SKTexture textureWithImageNamed:@"Pipe1"];
        _pipeTexture1.filteringMode = SKTextureFilteringNearest;
        _pipeTexture2 = [SKTexture textureWithImageNamed:@"Pipe2"];
        _pipeTexture2.filteringMode = SKTextureFilteringNearest;
        
        CGFloat distToMove = self.frame.size.width + 2 * _pipeTexture1.size.width;
        SKAction *movePipes = [SKAction moveByX:-distToMove y:0 duration:0.01 * distToMove];
        SKAction *removePipes = [SKAction removeFromParent];
        _moveAndRemovePipes = [SKAction sequence:@[movePipes, removePipes]];
        
        SKAction *spawn = [SKAction performSelector:@selector(spawnPipes) onTarget:self];
        SKAction *delay = [SKAction waitForDuration:2.0];
        SKAction *spawnThenDelay = [SKAction sequence:@[spawn, delay]];
        SKAction *spawnThenDelayForever = [SKAction repeatActionForever:spawnThenDelay];
        [self runAction:spawnThenDelayForever];
        
        //Bar
        _barTexture = [SKTexture textureWithImageNamed:@"Bar"];
        _barTexture.filteringMode = SKTextureFilteringNearest;
        
        CGFloat distToMoveBar = self.frame.size.width + 4 * _barTexture.size.width;
        SKAction *moveBars = [SKAction moveByX:-distToMoveBar y:0 duration:0.005 * distToMoveBar];
        SKAction *removeBars = [SKAction removeFromParent];
        _moveAndRemoveBars = [SKAction sequence:@[moveBars, removeBars]];
        
        SKAction *spawnB = [SKAction performSelector:@selector(spawnBars) onTarget:self];
        SKAction *delayB = [SKAction waitForDuration:1.0];
        SKAction *spawnThenDelayB = [SKAction sequence:@[spawnB, delayB]];
        SKAction *spawnThenDelayForeverB = [SKAction repeatActionForever:spawnThenDelayB];
        [self runAction:spawnThenDelayForeverB];
        
        //Initializing Score Label
        _score = 0;
        _scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Helvetica"];
        _scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), 3 * self.frame.size.height / 4);
        _scoreLabel.zPosition = 100;
        _scoreLabel.text = [NSString stringWithFormat:@"%ld", (long)_score];
        [self addChild:_scoreLabel];
        
    }
    return self;
}

- (void) spawnPipes {
    SKNode *pipePair = [SKNode node];
    pipePair.position = CGPointMake(self.frame.size.width + _pipeTexture1.size.width, 0);
    pipePair.zPosition = -10;
    
    CGFloat y = arc4random() % (NSInteger)(self.frame.size.height / 3);
    
    SKSpriteNode *pipe1 = [SKSpriteNode spriteNodeWithTexture:_pipeTexture1];
    [pipe1 setScale:2.0];
    pipe1.position = CGPointMake(0, y);
    pipe1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipe1.size];
    pipe1.physicsBody.dynamic = NO;
    pipe1.physicsBody.categoryBitMask = pipeCategory;
    pipe1.physicsBody.contactTestBitMask = flapperCategory;
    
    [pipePair addChild:pipe1];
    
    SKSpriteNode *pipe2 = [SKSpriteNode spriteNodeWithTexture:_pipeTexture2];
    [pipe2 setScale:2.0];
    pipe2.position = CGPointMake(0, y + pipe1.size.height + verticalPipeGap);
    pipe2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipe2.size];
    pipe2.physicsBody.dynamic = NO;
    pipe2.physicsBody.categoryBitMask = pipeCategory;
    pipe2.physicsBody.contactTestBitMask = flapperCategory;
    
    [pipePair addChild:pipe2];
    
    SKNode *contactNode = [SKNode node];
    contactNode.position = CGPointMake(pipe1.size.width + _flapper.size.width / 2, CGRectGetMidY(self.frame));
    contactNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(pipe2.size.width, self.frame.size.height)];
    contactNode.physicsBody.dynamic = NO;
    contactNode.physicsBody.categoryBitMask = scoreCategory;
    contactNode.physicsBody.contactTestBitMask = flapperCategory;
    
    [pipePair addChild:contactNode];
    
    [pipePair runAction:_moveAndRemovePipes];
    
    [_pipes addChild:pipePair];
}

- (void) spawnBars {
    
    CGFloat y = arc4random() % (NSInteger)(self.frame.size.height);
    
    SKSpriteNode *bar = [SKSpriteNode spriteNodeWithTexture:_barTexture];
    [bar setScale:2.0];
    bar.position = CGPointMake(self.frame.size.width, y);
    bar.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bar.size];
    bar.physicsBody.dynamic = NO;
    bar.physicsBody.categoryBitMask = pipeCategory;
    bar.physicsBody.contactTestBitMask = flapperCategory;
    
    [bar runAction:_moveAndRemoveBars];
    
    [_bars addChild:bar];
}

- (void) didBeginContact:(SKPhysicsContact *) contact {
    if (_moving.speed > 0) {
        if ((contact.bodyA.categoryBitMask & scoreCategory) == scoreCategory || (contact.bodyB.categoryBitMask & scoreCategory) == scoreCategory) {
            _score++;
            _scoreLabel.text = [NSString stringWithFormat:@"%ld", (long)_score];
            
            [_scoreLabel runAction:[SKAction sequence:@[[SKAction scaleTo:1.5 duration:0.1], [SKAction scaleTo:1.0 duration:0.1]]]];
        } else {
            _moving.speed = 0;
            
            _flapper.physicsBody.collisionBitMask = worldCategory;
            [_flapper runAction:[SKAction rotateByAngle:M_PI * _flapper.position.y * 0.01 duration:_flapper.position.y * 0.03]
                     completion:^{_flapper.speed = 0;}];
            
            [self removeActionForKey:@"flash"];
            [self runAction:[SKAction sequence:@[[SKAction repeatAction:[SKAction sequence:@[[SKAction runBlock:^{self.backgroundColor = [SKColor redColor];}],
                                                                                             [SKAction waitForDuration:0.05],
                                                                                             [SKAction runBlock:^{self.backgroundColor = _sky;}],
                                                                                             [SKAction waitForDuration:0.05]]] count:4],
                                                                                             [SKAction runBlock:^{_canRestart = YES;}]]] withKey:@"flash"];
        }
    }
}

- (void) resetScene {
    _flapper.position = CGPointMake(self.frame.size.width / 4, CGRectGetMidY(self.frame));
    _flapper.physicsBody.velocity = CGVectorMake(0, 0);
    _flapper.physicsBody.collisionBitMask = worldCategory | pipeCategory;
    _flapper.speed = 1.0;
    _flapper.zRotation = 0.0;
    
    [_pipes removeAllChildren];
    [_bars removeAllChildren];
    
    _canRestart = NO;
    
    _moving.speed = 1;
    
    _score = 0;
    _scoreLabel.text = [NSString stringWithFormat:@"%ld", (long)_score];
}

CGFloat turn(CGFloat min, CGFloat max, CGFloat val) {
    if (val > max) {
        return max;
    } else if (val < min) {
        return min;
    } else {
        return val;
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_moving.speed > 0) {
        _flapper.physicsBody.velocity = CGVectorMake(0, 0);
        [_flapper.physicsBody applyImpulse:CGVectorMake(0, 4)];
    } else if (_canRestart) {
        [self resetScene];
    }

}

-(void) update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    if (_moving.speed > 0) {
        _flapper.zRotation = turn(-1, 0.5, _flapper.physicsBody.velocity.dy * (_flapper.physicsBody.velocity.dy < 0 ? 0.003 : 0.001));
    }
}

@end

//
//  AppDelegate.h
//  p04-ferrara
//
//  Created by Dylan Ferrara on 3/12/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


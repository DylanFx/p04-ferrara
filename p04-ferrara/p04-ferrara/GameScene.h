//
//  GameScene.h
//  p04-ferrara
//
//  Created by Dylan Ferrara on 3/12/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>
@property (nonatomic) SKSpriteNode *flapper;
@property (nonatomic) SKColor *sky;
@property (nonatomic) SKTexture *pipeTexture1;
@property (nonatomic) SKTexture *pipeTexture2;
@property (nonatomic) SKTexture *barTexture;
@property (nonatomic) SKAction *moveAndRemovePipes;
@property (nonatomic) SKAction *moveAndRemoveBars;
@property (nonatomic) SKNode *moving;
@property (nonatomic) SKNode *pipes;
@property (nonatomic) SKNode *bars;
@property (nonatomic) SKLabelNode *scoreLabel;
@property (nonatomic) NSInteger score;
@property (nonatomic) BOOL canRestart;
@end
